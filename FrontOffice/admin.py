from django.contrib import admin

# Register your models here.
from FrontOffice.models import *

admin.site.register(Service)
admin.site.register(Invoice)
admin.site.register(Bill)
admin.site.register(Country)
admin.site.register(City)
admin.site.register(District)
admin.site.register(Language)
admin.site.register(AdType)
#admin.site.register(Ad)
admin.site.register(Property)

class AdPropertyAdmin(admin.ModelAdmin):
    fields = ('prop', 'ad', 'value')
admin.site.register(AdProperty, AdPropertyAdmin)

class AdPropertyInlines(admin.StackedInline):
    model = AdProperty
    extra = 1

class AdAdmin(admin.ModelAdmin):
    inlines = [AdPropertyInlines]
admin.site.register(Ad, AdAdmin)

admin.site.register(Photo)
admin.site.register(YoutubeVideo)
admin.site.register(Tag)
admin.site.register(Top)
admin.site.register(Currency)
admin.site.register(UserContact)