from django.db import models, connection
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from datetime import datetime
from FrontOffice.dictionary import PROPERTIES_MAP, AD_TYPE_NOUNS, AD_TYPE_VERBS
import re


class Service(models.Model):
    name = models.CharField(
        "Service Name",
        max_length=24,
        null=False
    )
    price = models.DecimalField(
        "Service Price",
        max_digits=10,
        decimal_places=2,
        null=False,
        default=0
    )

    class Meta:
        verbose_name = "Service"
        verbose_name_plural = "Services"

        unique_together = ("name", "price")

    def __unicode__(self):
        return "%s for %s" % (self.name, str(self.price))

    def save(self, *args, **kwargs):
        if self.name == '':
            self.name = None

        super(Service, self).save(*args, **kwargs)


class Invoice(models.Model):
    create_date = models.DateTimeField(
        "Created",
        null=False,
        default=datetime.now
    )
    mod_date = models.DateTimeField(
        "Modified",
        null=True,
        blank=True,
        default=None
    )
    discount_percents = models.PositiveSmallIntegerField(
        "Discount",
        null=False,
        default=0
    )
    STATUSES = (
        ("C", "Created"),
        ("P", "Pending"),
        ("X", "Cancelled"),
        ("R", "Rejected")
    )
    status = models.CharField(
        "Invoice Status",
        max_length=1,
        choices=STATUSES,
        null=False,
        default="C"
    )
    amount = models.DecimalField(
        "Invoice Amount",
        max_digits=10,
        decimal_places=2,
        null=True,
        default=None
    )

    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "Invoice"
        verbose_name_plural = "Invoices"

    def calc_bills_amount(self):
        amount = 0
        bills = Bill.objects.filter(invoice=self)
        for bill in bills:
            amount += bill.amount

        return amount

    def __unicode__(self):
        amount = \
            self.amount \
                if self.amount is not None \
                else self.calc_bills_amount()

        return "%s, $%d for %s [%s]" % \
               (
                   str(self.create_date),
                   amount,
                   self.user.username,
                   self.spell_status()
               )

    def save(self, *args, **kwargs):
        if self.status == "":
            self.status = None
        if self.amount == "":
            self.amount = None

        super(Invoice, self).save(*args, **kwargs)

    def set_status(self, status):
        allowed_statuses = dict(self.STATUSES)

        if status not in allowed_statuses.values():
            raise Exception("Undefined status: %s", status)

        inv_statuses = {v: k for k, v in allowed_statuses.items()}
        self.status = inv_statuses[status]

    def spell_status(self):
        allowed_statuses = dict(self.STATUSES)

        return allowed_statuses[self.status]


class Bill(models.Model):
    create_date = models.DateTimeField(
        "Created",
        null=False,
        default=datetime.now()
    )
    mod_date = models.DateTimeField(
        "Modified",
        null=True,
        default=None,
        blank=True
    )
    discount_percents = models.PositiveSmallIntegerField(
        "Discount",
        null=False,
        default=0
    )
    amount = models.FloatField(
        "Bill Amount",
        null=False,
        blank=False,
        default=0
    )

    service = models.ForeignKey(Service, null=True, on_delete=models.SET_NULL)
    invoice = models.ForeignKey(Invoice, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "Bill"
        verbose_name_plural = "Bills"

    def __unicode__(self):
        #return str(self.id) + " " + str(self.create_date) + " " + self.service.name
        return "%d %s %s" % (self.id, str(self.create_date), self.service.name)

    def save(self, *args, **kwargs):
        if self.amount == "":
            self.amount = None

        super(Bill, self).save(*args, **kwargs)


class Country(models.Model):
    code = models.CharField(
        "Country Code",
        max_length=2,
        null=False,
        blank=False,
        unique=True,
        help_text="2-letters code. Example: 'RU', 'KZ', 'US'"
    )

    class Meta:
        verbose_name = "Country"
        verbose_name_plural = "Countries"

    def __unicode__(self):
        return self.code

    def save(self, *args, **kwargs):
        if self.code == "":
            self.code = None
        if len(self.code) > 2:
            raise Exception('self.code is too long (max=2)')

        super(Country, self).save(*args, **kwargs)


class City(models.Model):
    name = models.CharField(
        "City Name",
        max_length=100,
        null=False,
        blank=False
    )

    country = models.ForeignKey(Country, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "City"
        verbose_name_plural = "Cities"
        unique_together = ("name", "country")

    def __unicode__(self):
        #return self.country.code + " > " + self.name
        return "%s > %s" % (self.country.code, self.name)
        #pass

    def save(self, *args, **kwargs):
        if self.name == "":
            self.name = None

        super(City, self).save(*args, **kwargs)


class District(models.Model):
    name = models.CharField(
        "District of the City",
        max_length=100,
        null=False,
        blank=False
    )

    city = models.ForeignKey(City, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "District"
        verbose_name_plural = "Districts"
        unique_together = ("name", "city")

    def __unicode__(self):
        return "%s > %s > %s" % (self.city.country.code, self.city.name, self.name)

    def save(self, *args, **kwargs):
        if self.name == "":
            self.name = None

        super(District, self).save(*args, **kwargs)


class Language(models.Model):
    code = models.CharField(
        "Language Code",
        max_length=3,
        null=False,
        blank=False,
        unique=True,
        help_text="3-letters code like: 'RUS', 'KAZ', 'ENG etc'"
    )

    class Meta:
        verbose_name = "Language"
        verbose_name_plural = "Languages"

    def __unicode__(self):
        return self.code

    def save(self, *args, **kwargs):
        if self.code == "":
            self.code = None
        if len(self.code) > 3:
            raise Exception("self.code is too long (max=3)")

        super(Language, self).save(*args, **kwargs)


class AdType(models.Model):
    verb = models.CharField(
        "Do",
        max_length=3,
        choices=AD_TYPE_VERBS,
        null=False,
        blank=False
    )
    noun = models.CharField(
        "What",
        max_length=3,
        choices=AD_TYPE_NOUNS,
        null=False,
        blank=False
    )

    class Meta:
        verbose_name = "Ad Type"
        verbose_name_plural = "Ad Types"
        unique_together = ("noun", "verb")

    def __unicode__(self):
        return "%s %s" % (self.spell_verb(), self.spell_noun())

    def save(self, *args, **kwargs):
        if self.noun == "":
            self.noun = None
        if self.verb == "":
            self.verb = None

        if self.noun not in (k for k, v in AD_TYPE_NOUNS):
            raise Exception("Noun is not allowed: " + self.noun)

        if self.verb not in (k for k, v in AD_TYPE_VERBS):
            raise Exception("Verb is not allowed: " + self.verb)

        super(AdType, self).save(*args, **kwargs)

    def spell_verb(self):
        verbs_dict = dict(AD_TYPE_VERBS)
        return verbs_dict[self.verb]

    def spell_noun(self):
        nouns_dict = dict(AD_TYPE_NOUNS)
        return nouns_dict[self.noun]


class Tag(models.Model):
    name = models.CharField(
        "Tag",
        max_length=50,
        null=False,
        unique=True,
        blank=False
    )
    price = models.DecimalField(
        "Tag Price",
        max_digits=10,
        decimal_places=2,
        null=False,
        default=0
    )

    class Meta:
        verbose_name = "Tag"
        verbose_name_plural = "Tags"

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.name == "":
            self.name = None

        super(Tag, self).save(*args, **kwargs)


class Ad(models.Model):
    # Meta
    summary = models.TextField(
        "Summary",
        null=True,
        default=None,
    )
    pub_date = models.DateTimeField(
        "Pub Date",
        null=False,
        default=datetime.now()
    )
    mod_date = models.DateTimeField(
        "Mod Date",
        null=True,
        default=None,
        blank=True
    )
    moderated = models.BooleanField(
        "Moderated",
        null=False,
        default=False
    )
    # Common
    price = models.DecimalField(
        "Price",
        max_digits=10,
        decimal_places=2,
        null=False,
        blank=False
    )
    misc = models.CharField(
        "Misc",
        max_length=100,
        null=True,
        default=None
    )
    has_arrest = models.NullBooleanField(
        "Has Arrest",
        null=True,
        default=None
    )
    has_pledge = models.NullBooleanField(
        "Has Pledge",
        null=True,
        default=None
    )
    has_encumbrance = models.NullBooleanField(
        "Has Encumbrance",
        null=True,
        default=None
    )
    ORIGINS = (
        ('1', 'owner'),
        ('2', 'developer'),
        ('3', 'realtor'),
        ('4', 'credit-organization')
    )
    origin = models.CharField(
        max_length=1,
        null=False,
        blank=False,
        choices=ORIGINS
    )

    # Foreign
    district = models.ForeignKey("District", null=True, on_delete=models.SET_NULL)
    ad_type = models.ForeignKey("AdType", null=True, on_delete=models.SET_NULL)
    language = models.ForeignKey("Language", null=True, on_delete=models.SET_NULL)
    user_contact = models.ForeignKey("UserContact", null=True, on_delete=models.SET_NULL)

    tags = models.ManyToManyField(Tag)

    class Meta:
        verbose_name = "Ad"
        verbose_name_plural = "Ads"

    def __unicode__(self):
        return "%s, [%s]" % (self.summary, self.misc)

    def save(self, *args, **kwargs):
        if self.summary == "":
            self.summary = None
        if self.moderated == "":
            self.moderated = None
        if self.misc == "":
            self.misc = None
        if self.has_encumbrance == "":
            self.has_encumbrance = None
        if self.has_arrest == "":
            self.has_arrest = None
        if self.has_pledge == "":
            self.has_pledge = None
        if self.origin == "":
            self.origin = None

        # check price format: %.2f
        match = re.search(r'\.(\d+)$', str(self.price))
        #print('group2: %s' % match.group(1))
        if match and len(match.group(1)) > 2:
            raise Exception("Invalid price: %s" % self.price)

        super(Ad, self).save(*args, **kwargs)

    def _fetch_properties(self):
        SQL = """
            SELECT
                p.name,
                ap.value
            FROM FrontOffice_adproperty ap
            JOIN FrontOffice_property p ON ap.prop_id = p.id
            WHERE
                ap.ad_id=%s
        """
        cursor = connection.cursor()
        cursor.execute(SQL, [self.id])
        for name, value in cursor.fetchall():
            yield (name, value)

    def list_properties(self):
        properties = list()
        for name, value in self._fetch_properties():
            properties.append({"name": name, "value": value})

        return properties

    def list_properties_evaluated(self):
        properties = list()
        for name, value in self._fetch_properties():
            properties.append({
                "name": name,
                "value": Property.evaluate(name, value)
            })

        return properties


class Property(models.Model):
    name = models.CharField(
        "Property Name",
        max_length=50,
        null=False,
        unique=True,
    )

    RUS = models.CharField(
        "Russian Desc",
        max_length=120,
        null=True
    )

    CONT_TYPES = (
        ('NUM', 'Number'),
        ('ENUM', 'String'),
        ('BOOL', 'Boolean')
    )
    cont_type = models.CharField(
        "Content Type",
        max_length=4,
        choices=CONT_TYPES,
        null=False,
        blank=False
    )

    ad_types = models.ManyToManyField(AdType)

    class Meta:
        verbose_name = "Property"
        verbose_name_plural = "Properties"

    def __unicode__(self):
        return self.RUS if self.RUS else self.name

    def save(self, *args, **kwargs):
        if self.name == "":
            self.name = None

        if self.name not in PROPERTIES_MAP.keys():
            raise Exception("Not allowed property name: %s" % (self.name))

        if self.cont_type not in (k for k, v in self.CONT_TYPES):
            raise Exception("Wrong Content Type of Property: " + self.cont_type)

        super(Property, self).save(*args, **kwargs)

    @classmethod
    def evaluate(cls, property_name, property_value):
        property_type = PROPERTIES_MAP[property_name]['type']
        if property_type == "NUM":
            try:
                num = float(property_value)
                return num
            except ValueError:
                return None

        elif property_type == "BOOL":
            if property_value not in ['0', '1']:
                return None
            return 'yes' if property_value == '1' else 'no'

        elif property_type == "ENUM":
            return PROPERTIES_MAP[property_name]['values'][str(property_value)]

        else:
            return None


class AdProperty(models.Model):
    value = models.CharField(
        "Ad Property Value",
        max_length=10,
        null=False,
        blank=False
    )

    ad = models.ForeignKey(Ad, on_delete=models.CASCADE)
    prop = models.ForeignKey(Property, null=True, on_delete=models.SET_NULL)

    def __unicode__(self):
        return "%s = %s, %s" % (self.prop.name, self.value, self.ad.summary)

    class Meta:
        verbose_name = "Ad Property"
        verbose_name_plural = "Ad Properties"

        unique_together = ("ad", "prop")

    def save(self, *args, **kwargs):
        if Property.evaluate(self.prop.name, self.value) is None:
            raise ValidationError("Invalid value for type %s" % self.prop.cont_type)

        super(AdProperty, self).save(*args, **kwargs)


class Photo(models.Model):
    filename = models.CharField(
        "Photo file",
        max_length=32,
        null=False
    )

    ad = models.ForeignKey(Ad, on_delete=models.CASCADE)

    def __unicode__(self):
        return "Photo for \"%s\" (\"%s\")" % (self.ad.summary, self.ad.misc)

    class Meta:
        verbose_name = "Photo"
        verbose_name_plural = "Photos"

    def save(self, *args, **kwargs):
        if self.filename == "":
            self.filename = None

        super(Photo, self).save(*args, **kwargs)


class YoutubeVideo(models.Model):
    widget_code = models.TextField(
        "Widget Code",
        null=False,
        blank=False
    )

    ad = models.ForeignKey(Ad, on_delete=models.CASCADE)

    def __unicode__(self):
        return "Video for \"%s\" (\"%s\")" % (self.ad.summary, self.ad.misc)

    class Meta:
        verbose_name = "Youtube Video"
        verbose_name_plural = "Youtube Videos"

    def save(self, *args, **kwargs):
        if self.widget_code == "":
            self.widget_code = None

        super(YoutubeVideo, self).save(*args, **kwargs)


class Top(models.Model):
    start_date = models.DateField(
        "Start Date",
        null=False,
        default=datetime.now()
    )
    finish_date = models.DateField(
        "Finish Date",
        null=True,
        default=None,
        blank=True
    )

    ad = models.ForeignKey(Ad, on_delete=models.CASCADE)
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE)

    def __unicode__(self):
        return "%s <> %s" % (str(self.start_date), str(self.finish_date))

    class Meta:
        verbose_name = "Top"
        verbose_name_plural = "Tops"
        unique_together = ("start_date", "ad")


class Currency(models.Model):
    code = models.CharField(
        "Code",
        max_length=3,
        null=False,
        unique=True,
        help_text="3-letters code. Example: 'RUR', 'KZT', 'USD'"
    )
    rate = models.DecimalField(
        "Rate",
        max_digits=10,
        decimal_places=2,
        null=True,
        default=None,
        help_text="Amount in USD"
    )

    class Meta:
        verbose_name = "Currency"
        verbose_name_plural = "Currencies"

    def __unicode__(self):
        return self.code

    def save(self, *args, **kwargs):
        if self.code == "":
            self.code = None
        if len(self.code) > 3:
            raise Exception("code is too long (max=3)")

        super(Currency, self).save(*args, **kwargs)


class UserContact(models.Model):
    phone_number = models.CharField(
        "Phone Number",
        max_length=50,
        null=None
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "User Contact"
        verbose_name_plural = "User Contacts"
        unique_together = ("phone_number", "user")

    def __unicode__(self):
        return "%s - %s" % (self.phone_number, self.user.username)

    def save(self, *args, **kwargs):
        if self.phone_number == "":
            self.phone_number = None

        super(UserContact, self).save(*args, **kwargs)
