from django.test import TestCase
from django.contrib.auth.models import User

from decimal import Decimal
from FrontOffice.models import *


class LanguageTest(TestCase):
    def setUp(self):
        Language.objects.create(code="RUS")
        Language.objects.create(code="KAZ")

    def test_language_has_code(self):
        rus_lang = Language.objects.get(code="RUS")
        self.assertEqual(rus_lang.code, "RUS")

        kaz_lang = Language.objects.get(code="KAZ")
        self.assertEqual(kaz_lang.code, "KAZ")

    def test_language_is_unique(self):
        with self.assertRaises(Exception):
            Language.objects.create(code="RUS")

    def test_language_is_not_null(self):
        with self.assertRaises(Exception):
            Language.objects.create(code=None)

    def test_language_is_not_empty(self):
        with self.assertRaises(Exception):
            Language.objects.create(code="")

    def test_language_too_long_code(self):
        with self.assertRaises(Exception):
            Language.objects.create(code="TOO_LONG_CODE")

    def test_language_unicode_call(self):
        rus_lang = Language.objects.get(code="RUS")
        self.assertEqual(rus_lang.__unicode__(), "RUS")


class CountryTest(TestCase):
    def setUp(self):
        Country.objects.create(code="KZ")
        Country.objects.create(code="RU")

    def test_country_has_code(self):
        russia = Country.objects.get(code="RU")
        self.assertEqual(russia.code, "RU")

        kaz = Country.objects.get(code="KZ")
        self.assertEqual(kaz.code, "KZ")

    def test_country_is_unique(self):
        with self.assertRaises(Exception):
            Country.objects.create(code="RU")

    def test_country_is_not_null(self):
        with self.assertRaises(Exception):
            Country.objects.create(code=None)

    def test_country_is_not_empty(self):
        with self.assertRaises(Exception):
            Country.objects.create(code="")

    def test_country_too_long_code(self):
        with self.assertRaises(Exception):
            Country.objects.create(code="TOO_LONG_CODE")

    def test_country_unicode_call(self):
        russia = Country.objects.get(code="RU")
        self.assertEqual(russia.__unicode__(), "RU")


class ServiceTest(TestCase):
    def setUp(self):
        Service.objects.create(name='common', price=10.01)
        Service.objects.create(name='default_price')

    def test_service_name(self):
        s = Service.objects.get(name='common')
        self.assertEqual(s.name, 'common')

    def test_service_price(self):
        s = Service.objects.get(name='common')
        self.assertEqual(s.price, Decimal('10.01'))

    def test_service_default_price(self):
        s = Service.objects.get(name='default_price')
        self.assertEqual(s.price, Decimal('0'))

    def test_service_add_same_name_diff_price(self):
        self.assertTrue(Service.objects.create(name='common'))

    def test_service_two_with_same_names(self):
        Service.objects.create(name='common')
        commons = Service.objects.filter(name='common')

        self.assertEqual(commons[0].name, 'common')
        self.assertEqual(commons[1].name, 'common')

        self.assertTrue(commons[0].price in [Decimal('10.01'), Decimal('0')])
        self.assertTrue(commons[1].price in [Decimal('10.01'), Decimal('0')])

    def test_service_unicode_call(self):
        s = Service.objects.get(name='common')
        self.assertEqual(s.__unicode__(), 'common for 10.01')

    def test_service_not_null_name(self):
        with self.assertRaises(Exception):
            Service.objects.create(name=None)

    def test_service_empty_string_name(self):
        with self.assertRaises(Exception):
            Service.objects.create(name='')

    def test_service_unique_name_and_price(self):
        with self.assertRaises(Exception):
            Service.objects.create(name='common', price='10.01')

    def test_service_empty_string_price(self):
        with self.assertRaises(Exception):
            Service.objects.create(name='service', price='')

    def test_service_null_price(self):
        with self.assertRaises(Exception):
            Service.objects.create(name='service', price=None)


class InvoiceTest(TestCase):
    test_user = None

    def setUp(self):
        test_user = User.objects.create(username='user', password='123')
        self.test_user = test_user

        Invoice.objects.create(amount=99, user=test_user)

    def test_invoice_create(self):
        self.assertTrue(Invoice.objects.create(amount=100, user=self.test_user))

    def test_invoice_amount(self):
        i = Invoice.objects.get(pk=1)
        self.assertEqual(i.amount, Decimal(99))

    def test_invoice_created_status(self):
        i = Invoice.objects.get(pk=1)
        self.assertEqual(i.status, 'C')

    def test_invoice_set_status(self):
        i = Invoice.objects.get(pk=1)
        i.set_status("Pending")
        self.assertEqual(i.status, "P")

    def test_invoice_set_wrong_status(self):
        i = Invoice.objects.get(pk=1)
        with self.assertRaises(Exception):
            i.set_status("Wrong Status")

    def test_invoice_set_empty_status(self):
        i = Invoice.objects.get(pk=1)
        with self.assertRaises(Exception):
            i.set_status()

    def test_invoice_spell_status(self):
        i = Invoice.objects.get(pk=1)
        i.set_status("Cancelled")
        self.assertEqual(i.status, "X")
        self.assertEqual(i.spell_status(), "Cancelled")

    def test_invoice_unicode_call(self):
        i = Invoice.objects.create(
            user = self.test_user
        )
        s = Service.objects.create(
            name='TEST',
            price=1
        )
        self.assertEqual(i.amount, None)
        Bill.objects.create(
            amount=99.99,
            invoice=i,
            service=s
        )
        Bill.objects.create(
            amount=0.01,
            invoice=i,
            service=s
        )
        self.assertEqual(i.calc_bills_amount(), 100)

        self.assertEqual(
            i.__unicode__(),
            str(i.create_date) + ", $100" + " for " +
            self.test_user.username + " [Created]"
        )

    def test_invoice_status_save_null(self):
        i = Invoice.objects.get(pk=1)
        i.status = None

        with self.assertRaises(Exception):
            i.save()

    def test_invoice_status_save_empty_string(self):
        i = Invoice.objects.get(pk=1)
        i.status = ""

        with self.assertRaises(Exception):
            i.save()

    def test_invoice_amount_save_empty_string(self):
        i = Invoice.objects.get(pk=1)
        i.amount = ""
        i.save()
        i2 = Invoice.objects.get(pk=1)
        self.assertEqual(i2.amount, None)


class BillTest(TestCase):
    bill = None
    test_user = None
    test_service = None
    test_invoice = None

    def setUp(self):
        self.test_user = User.objects.create(
            username='TestUser',
            password='123'
        )
        self.test_invoice = Invoice.objects.create(
            amount=99.99,
            user=self.test_user
        )
        self.test_service = Service.objects.create(
            name='Test Service Name',
            price=99.98
        )

        self.bill = Bill.objects.create(
            service=self.test_service,
            invoice=self.test_invoice,
            amount=10.30
        )

    def test_top_defaults(self):
        bill = Bill.objects.get(pk=1)
        self.assertEqual(bill.mod_date, None)
        self.assertEqual(bill.discount_percents, 0)
        self.assertIsNotNone(bill.create_date)

    def test_bill_unicode_call(self):
        bill = Bill.objects.get(pk=1)
        self.assertEqual(
            bill.__unicode__(),
            "1 " + str(bill.create_date) + " " + bill.service.name
        )

    def test_bill_amount_save_null(self):
        with self.assertRaises(Exception):
            Bill.objects.create(
                service=self.test_service,
                invoice=self.test_invoice,
                amount=None
            )

    def test_bill_amount_save_blank(self):
        with self.assertRaises(Exception):
            Bill.objects.create(
                service=self.test_service,
                invoice=self.test_invoice,
                amount=""
            )


class CityTest(TestCase):
    test_country = None

    def setUp(self):
        test_country = Country.objects.create(code="KZ")
        self.test_country = test_country
        city = City.objects.create(name="Astana", country=test_country)

    def test_city_created(self):
        city = City.objects.get(pk=1)
        self.assertEqual(city.name, "Astana")
        self.assertEqual(city.country.code, "KZ")

    def test_city_name_save_null(self):
        city = City.objects.get(pk=1)
        city.name = None
        with self.assertRaises(Exception):
            city.save()

    def test_city_name_save_null(self):
        city = City.objects.get(pk=1)
        city.name = ""
        with self.assertRaises(Exception):
            city.save()

    def test_city_save_same_city_another_country(self):
        city = City.objects.get(pk=1)
        cnt = Country.objects.create(code="RU")
        self.assertTrue(City.objects.create(name=city.name, country=cnt))

    def test_city_save_same_city_same_country(self):
        city = City.objects.get(pk=1)
        with self.assertRaises(Exception):
            City.objects.create(name=city.name, country=city.country)

    def test_city_unicode_call(self):
        city = City.objects.get(pk=1)
        self.assertEqual(city.__unicode__(), city.country.code + ' > ' + city.name)


class DistrictTest(TestCase):
    cnt = None

    def setUp(self):
        cnt = Country.objects.create(code="KZ")
        self.cnt = cnt
        city = City.objects.create(name="Astana", country=cnt)
        District.objects.create(name="D", city=city)

    def test_district_created(self):
        d = District.objects.get(pk=1)
        self.assertEqual(d.name, "D")
        self.assertEqual(d.city.name, "Astana")
        self.assertEqual(d.city.country.code, "KZ")

    def test_district_save_name_as_null(self):
        d = District.objects.get(pk=1)
        d.name = None
        with self.assertRaises(Exception):
            d.save()

    def test_district_save_name_as_null(self):
        d = District.objects.get(pk=1)
        d.name = ""
        with self.assertRaises(Exception):
            d.save()

    def test_district_create_with_same_name_different_city(self):
        d = District.objects.get(pk=1)
        c = City.objects.create(name="Aktau", country=self.cnt)
        self.assertTrue(District.objects.create(name=d.name, city=c))

    def test_district_create_with_same_name_same_city(self):
        d = District.objects.get(pk=1)
        with self.assertRaises(Exception):
            District.objects.create(name=d.name, city=d.city)

    def test_district_unicode_call(self):
        d = District.objects.get(pk=1)
        self.assertEqual(d.__unicode__(), "KZ > Astana > D")


class AdTypeTest(TestCase):
    def setUp(self):
        AdType.objects.create(verb="SEL", noun="FLT")

    def test_adtype_created(self):
        t = AdType.objects.get(pk=1)
        self.assertEqual(t.verb, "SEL")
        self.assertEqual(t.noun, "FLT")

    def test_adtype_spell_verb(self):
        t = AdType.objects.get(pk=1)
        self.assertEqual(t.spell_verb(), "Sell")

    def test_adtype_spell_noun(self):
        t = AdType.objects.get(pk=1)
        self.assertEqual(t.spell_noun(), "Flat")

    def test_adtype_create_with_all_null_params(self):
        with self.assertRaises(Exception):
            AdType.objects.create(noun=None, verb=None)

    def test_adtype_create_with_all_empty_string_params(self):
        with self.assertRaises(Exception):
            AdType.objects.create(noun="", verb="")

    def test_adtype_create_with_all_no_params(self):
        with self.assertRaises(Exception):
            AdType.objects.create()

    def test_adtype_create_with_invalid_noun(self):
        with self.assertRaises(Exception):
            AdType.objects.create(noun="XXX", verb="SEL")

    def test_adtype_create_with_invalid_verb(self):
        with self.assertRaises(Exception):
            AdType.objects.create(noun="BLD", verb="XXX")

    def test_adttype_create_not_unique(self):
        t = AdType.objects.get(pk=1)
        with self.assertRaises(Exception):
            AdType.objects.create(noun=t.noun, verb=t.verb)

    def test_adtype_unicode_call(self):
        t = AdType.objects.get(pk=1)
        self.assertEqual(t.__unicode__(), "Sell Flat")


class TagTest(TestCase):
    def setUp(self):
        Tag.objects.create(name="hello")

    def test_tag_created_defaults(self):
        t = Tag.objects.get(pk=1)
        self.assertEqual(t.name, "hello")
        self.assertEqual(t.price, 0)

    def test_tag_save_null_name(self):
        t = Tag.objects.get(pk=1)
        t.name = None
        with self.assertRaises(Exception):
            t.save()

    def test_tag_save_null_price(self):
        t = Tag.objects.get(pk=1)
        t.price = None
        with self.assertRaises(Exception):
            t.save()

    def test_tag_save_empty_str_name(self):
        t = Tag.objects.get(pk=1)
        t.name = ""
        with self.assertRaises(Exception):
            t.save()

    def test_tag_save_empty_str_price(self):
        t = Tag.objects.get(pk=1)
        t.price = ""
        with self.assertRaises(Exception):
            t.save()

    def test_tag_save_not_uique(self):
        t = Tag.objects.get(pk=1)
        with self.assertRaises(Exception):
            Tag.objects.create(name=t.name)

    def test_tag_unicode_call(self):
        t = Tag.objects.get(pk=1)
        self.assertEqual(t.name, "hello")


class AdTest(TestCase):
    language = None
    ad_type = None
    district = None
    country = None
    city = None
    user = None
    user_contact = None
    property = None
    ad_property = None
    ad = None

    def setUp(self):
        self.language = Language.objects.create(code="RUS")
        self.ad_type = AdType.objects.create(
            verb="SEL",
            noun="FLT"
        )
        self.country = Country.objects.create(code="KZ")
        self.city = City.objects.create(
            name="Astana",
            country=self.country
        )
        self.district = District.objects.create(
            name="Center",
            city=self.city
        )
        self.user = User.objects.create(
            username="Test",
            password="123"
        )
        self.user_contact = UserContact.objects.create(
            phone_number="911",
            user=self.user
        )
        self.property = Property.objects.create(
            name='has-garage',
            cont_type='BOOL',
            #ad_type=self.ad_type
        )
        self.property.ad_types.add(self.ad_type)

        self.ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            origin='1'
        )

        self.ad_property = AdProperty.objects.create(
            prop=self.property,
            ad=self.ad,
            value='0'
        )

    def test_ad_created(self):
        ad = Ad.objects.get(pk=1)

        self.assertEqual(ad.price, Decimal('99.99'))

        self.assertEqual(ad.district.name, 'Center')
        self.assertEqual(ad.district.city.name, 'Astana')
        self.assertEqual(ad.district.city.country.code, 'KZ')

        self.assertEqual(ad.ad_type.verb, 'SEL')
        self.assertEqual(ad.ad_type.noun, 'FLT')

        self.assertEqual(ad.language.code, 'RUS')

        self.assertEqual(ad.user_contact.phone_number, '911')
        self.assertEqual(ad.user_contact.user.username, 'Test')

        self.assertEqual(ad.moderated, False)
        self.assertEqual(ad.misc, None)
        self.assertEqual(ad.has_arrest, None)
        self.assertEqual(ad.has_pledge, None)
        self.assertEqual(ad.has_encumbrance, None)

        self.assertEqual(ad.summary, None)
        self.assertEqual(ad.mod_date, None)

    # Rouine:
    def test_ad_create_with_summary_is_blank(self):
        ad = Ad.objects.create(
            summary="",
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            origin='1',
        )
        self.assertEqual(ad.summary, None)

    def test_ad_create_with_mod_date_is_blank(self):
        with self.assertRaises(Exception) as e:
            Ad.objects.create(
                summary="",
                price=99.99,
                district=self.district,
                ad_type=self.ad_type,
                language=self.language,
                user_contact=self.user_contact,
                mod_date="",
                origin='1',
            )

    def test_ad_create_with_moderated_is_None(self):
        with self.assertRaises(Exception) as e:
            Ad.objects.create(
                price=99.99,
                district=self.district,
                ad_type=self.ad_type,
                language=self.language,
                user_contact=self.user_contact,
                moderated=None,
                origin='1'
            )

    def test_ad_create_with_moderated_is_blank(self):
        with self.assertRaises(Exception) as e:
            Ad.objects.create(
                price=99.99,
                district=self.district,
                ad_type=self.ad_type,
                language=self.language,
                user_contact=self.user_contact,
                moderated="",
                origin='1'
            )

    def test_ad_create_with_moderated_is_True(self):
        ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            moderated=True,
            origin='1'
        )
        self.assertEqual(ad.moderated, True)

    def test_ad_create_with_moderated_is_False(self):
        ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            moderated=False,
            origin='1'
        )
        self.assertEqual(ad.moderated, False)

    def test_ad_create_with_price_is_None(self):
        with self.assertRaises(Exception) as e:
            Ad.objects.create(
                price=None,
                district=self.district,
                ad_type=self.ad_type,
                language=self.language,
                user_contact=self.user_contact,
                origin='1'
            )

    def test_ad_create_with_price_is_blank(self):
        with self.assertRaises(Exception) as e:
            Ad.objects.create(
                price="",
                district=self.district,
                ad_type=self.ad_type,
                language=self.language,
                user_contact=self.user_contact,
                origin='1'
            )

    def test_ad_create_with_price_is_long_format(self):
        with self.assertRaises(Exception):
            Ad.objects.create(
                price=99.999,
                district=self.district,
                ad_type=self.ad_type,
                language=self.language,
                user_contact=self.user_contact,
                origin='1'
            )

    def test_ad_create_with_misc_is_None(self):
        ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            moderated=False,
            misc=None,
            origin='1'
        )
        self.assertEqual(ad.misc, None)

    def test_ad_create_with_misc_is_blank(self):
        ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            moderated=False,
            misc="",
            origin='1'
        )
        self.assertEqual(ad.misc, None)

    def test_ad_create_with_has_arrest_is_blank(self):
        ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            moderated=False,
            has_arrest="",
            origin='1'
        )
        self.assertEqual(ad.has_arrest, None)

    def test_ad_create_with_has_arrest_is_True(self):
        ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            moderated=False,
            has_arrest=True,
            origin='1'
        )
        self.assertEqual(ad.has_arrest, True)

    def test_ad_create_with_has_pledge_is_blank(self):
        ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            moderated=False,
            has_pledge="",
            origin='1'
        )
        self.assertEqual(ad.has_pledge, None)

    def test_ad_create_with_has_pledge_is_True(self):
        ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            moderated=False,
            has_pledge=True,
            origin='1'
        )
        self.assertEqual(ad.has_pledge, True)

    def test_ad_create_with_has_encumbrance_is_blank(self):
        ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            moderated=False,
            has_encumbrance="",
            origin='1'
        )
        self.assertEqual(ad.has_encumbrance, None)

    def test_ad_create_with_has_encumbrance_is_True(self):
        ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            moderated=False,
            has_encumbrance=True,
            origin='1'
        )
        self.assertEqual(ad.has_encumbrance, True)

    # business-logic
    def test_ad_list_properties(self):
        lp = self.ad.list_properties()
        self.assertTrue(len(lp) == 1)
        self.assertDictEqual(lp[0], {'name': 'has-garage', 'value': '0'})

    def test_ad_list_properties_evaluated(self):
        lp = self.ad.list_properties_evaluated()
        self.assertTrue(len(lp) == 1)
        self.assertDictEqual(lp[0], {'name': 'has-garage', 'value': 'no'})

    def test_ad_add_tags(self):
        t1 = Tag.objects.create(
            name='Tag-1',
            price=1.50
        )
        t2 = Tag.objects.create(
            name='Tag-2',
            price=2.99
        )
        self.ad.tags.add(t1, t2)

        all_tags = self.ad.tags.all()
        self.assertTrue(len(all_tags) == 2)
        self.assertEqual(all_tags[0].name, 'Tag-1')
        self.assertEqual(all_tags[1].name, 'Tag-2')


class PropertyTest(TestCase):
    ad_type = None
    prop = None

    def setUp(self):
        self.ad_type = AdType.objects.create(verb="SEL", noun="FLT")
        self.prop = Property.objects.create(
            name="has-telephone",
            cont_type="NUM",
            #ad_types=self.ad_type
        )
        self.prop.ad_types.add(self.ad_type)

    def test_property_default_created(self):
        p = Property.objects.get(pk=1)
        self.assertEqual(p.name, "has-telephone")
        self.assertEqual(p.cont_type, "NUM")

    def test_property_create_with_null_name(self):
        with self.assertRaises(Exception):
            Property.objects.create(
                name=None,
                cont_type="NUM",
                ad_type=self.ad_type
            )

    def test_property_create_with_blank_name(self):
        with self.assertRaises(Exception):
            Property.objects.create(
                name="",
                cont_type="NUM",
                ad_type=self.ad_type
            )

    def test_property_create_with_wrong_cont_type(self):
        with self.assertRaises(Exception):
            Property.objects.create(
                name="has-telephone",
                cont_type="WRONG",
                ad_type=self.ad_type
            )

    def test_property_create_with_null_cont_type(self):
        with self.assertRaises(Exception):
            Property.objects.create(
                name="has-telephone",
                cont_type=None,
                ad_type=self.ad_type
            )

    def test_property_create_with_blank_cont_type(self):
        with self.assertRaises(Exception):
            Property.objects.create(
                name="has-telephone",
                cont_type="",
                ad_type=self.ad_type
            )

    def test_property_create_with_not_allowed_name(self):
        with self.assertRaises(Exception):
            Property.objects.create(name="WRONG", cont_type="NUM")

    def test_property_create_not_unique(self):
        p = Property.objects.get(pk=1)
        with self.assertRaises(Exception):
            Property.objects.create(name=p.name, cont_type="BOOL")

    def test_property_unicode_call(self):
        p = Property.objects.get(pk=1)
        self.assertEqual(p.__unicode__(), "has-telephone")

    def test_property_evaluate(self):
        self.assertEqual(Property.evaluate('type-of-garage', '1'), 'wood')
        self.assertEqual(Property.evaluate('land-area', '102'), 102)
        self.assertEqual(Property.evaluate('has-guard', '1'), 'yes')
        self.assertEqual(Property.evaluate('has-guard', '2'), None)

        with self.assertRaises(Exception):
            Property.evaluate('DUMMY-KEY', 1)

        with self.assertRaises(Exception):
            Property.evaluate('has-guard')  # not enough args


class AdPropertyTest(TestCase):
    ad = None
    prop = None

    def setUp(self):
        self.language = Language.objects.create(code="RUS")
        self.ad_type = AdType.objects.create(
            verb="SEL",
            noun="FLT"
        )
        self.country = Country.objects.create(code="KZ")
        self.city = City.objects.create(
            name="Astana",
            country=self.country
        )
        self.district = District.objects.create(
            name="Center",
            city=self.city
        )
        self.user = User.objects.create(
            username="Test",
            password="123"
        )
        self.user_contact = UserContact.objects.create(
            phone_number="911",
            user=self.user
        )
        self.ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            origin='1'
        )
        self.prop = Property.objects.create(
            name="has-telephone",
            cont_type="BOOL",
            #ad_types=self.ad_type
        )
        self.prop.ad_types.add(self.ad_type)

        AdProperty.objects.create(
            ad=self.ad,
            prop=self.prop,
            value='1'
        )

    def test_adproperty_created(self):
        ap = AdProperty.objects.get(pk=1)
        self.assertEqual(ap.ad.price, Decimal('99.99'))
        self.assertEqual(ap.prop.name, 'has-telephone')
        self.assertEqual(ap.value, '1')

    def test_adproperty_create_not_unique(self):
        with self.assertRaises(Exception):
            AdProperty.objects.create(
                ad=self.ad,
                prop=self.prop,
                value='1'
            )

    def test_adproperty_create_with_value_is_None(self):
        ad_type = AdType.objects.create(
            verb="SEL",
            noun="HOM"
        )
        p = Property.objects.create(
            name='has-gas',
            cont_type="BOOL",
        )
        p.ad_types.add(ad_type)
        with self.assertRaises(Exception):
            AdProperty.objects.create(
                ad=self.ad,
                prop=p,
                value=None
            )

    def test_adproperty_create_with_value_is_blank(self):
        ad_type = AdType.objects.create(
            verb="SEL",
            noun="HOM"
        )
        p = Property.objects.create(
            name='has-gas',
            cont_type="BOOL",
        )
        p.ad_types.add(ad_type)
        with self.assertRaises(Exception):
            AdProperty.objects.create(
                ad=self.ad,
                prop=p,
                value=""
            )

    def test_adproperty_create_with_value_wrong_type(self):
        ad_type = AdType.objects.create(
            verb="SEL",
            noun="HOM"
        )
        p = Property.objects.create(
            name='has-gas',
            cont_type="BOOL",
            #ad_type=ad_type
        )
        p.ad_types.add(ad_type)
        with self.assertRaises(Exception):
            AdProperty.objects.create(
                ad=self.ad,
                prop=p,
                value=123.40
            )


class PhotoTest(TestCase):
    ad = None

    def setUp(self):
        self.language = Language.objects.create(code="RUS")
        self.ad_type = AdType.objects.create(
            verb="SEL",
            noun="FLT"
        )
        self.country = Country.objects.create(code="KZ")
        self.city = City.objects.create(
            name="Astana",
            country=self.country
        )
        self.district = District.objects.create(
            name="Center",
            city=self.city
        )
        self.user = User.objects.create(
            username="Test",
            password="123"
        )
        self.user_contact = UserContact.objects.create(
            phone_number="911",
            user=self.user
        )
        self.ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            origin='1',
        )
        Photo.objects.create(
            filename='12345678901234567890123456789012',
            ad=self.ad
        )

    def test_photo_created(self):
        p = Photo.objects.get(pk=1)
        self.assertEqual(p.filename, '12345678901234567890123456789012')

    def test_photo_create_with_filename_is_None(self):
        with self.assertRaises(Exception):
            Photo.objects.create(
                filename=None,
                ad=self.ad
            )

    def test_photo_create_with_filename_is_blank(self):
        with self.assertRaises(Exception):
            Photo.objects.create(
                filename="",
                ad=self.ad
            )

    def test_photo_create_with_filename_is_empty(self):
        with self.assertRaises(Exception):
            Photo.objects.create(
                ad=self.ad
            )


class YoutubeVideoTest(TestCase):
    ad = None

    def setUp(self):
        self.language = Language.objects.create(code="RUS")
        self.ad_type = AdType.objects.create(
            verb="SEL",
            noun="FLT"
        )
        self.country = Country.objects.create(code="KZ")
        self.city = City.objects.create(
            name="Astana",
            country=self.country
        )
        self.district = District.objects.create(
            name="Center",
            city=self.city
        )
        self.user = User.objects.create(
            username="Test",
            password="123"
        )
        self.user_contact = UserContact.objects.create(
            phone_number="911",
            user=self.user
        )
        self.ad = Ad.objects.create(
            price=99.99,
            district=self.district,
            ad_type=self.ad_type,
            language=self.language,
            user_contact=self.user_contact,
            origin='1'
        )
        YoutubeVideo.objects.create(
            widget_code='hello!',
            ad=self.ad
        )

    def test_YV_created(self):
        y = YoutubeVideo.objects.get(pk=1)
        self.assertEqual(y.widget_code, 'hello!')

    def test_YV_create_with_widget_code_is_None(self):
        with self.assertRaises(Exception):
            YoutubeVideo.objects.create(
                widget_code=None,
                ad=self.ad
            )

    def test_YV_create_with_widget_code_is_blank(self):
        with self.assertRaises(Exception):
            YoutubeVideo.objects.create(
                widget_code="",
                ad=self.ad
            )

    def test_YV_create_with_widget_code_is_empty(self):
        with self.assertRaises(Exception):
            YoutubeVideo.objects.create(
                ad=self.ad
            )


class TopTest(TestCase):
    ad = None
    bill = None

    def setUp(self):
        language = Language.objects.create(code="RUS")
        ad_type = AdType.objects.create(
            verb="SEL",
            noun="FLT"
        )
        country = Country.objects.create(code="KZ")
        city = City.objects.create(
            name="Astana",
            country=country
        )
        district = District.objects.create(
            name="Center",
            city=city
        )
        user = User.objects.create(
            username="Test",
            password="123"
        )
        user_contact = UserContact.objects.create(
            phone_number="911",
            user=user
        )
        self.ad = Ad.objects.create(
            price=99.99,
            district=district,
            ad_type=ad_type,
            language=language,
            user_contact=user_contact,
            origin='1',
        )

        invoice = Invoice.objects.create(
            amount=99.99,
            user=user
        )
        service = Service.objects.create(
            name='Test Service Name',
            price=99.98
        )

        self.bill = Bill.objects.create(
            service=service,
            invoice=invoice
        )

        Top.objects.create(
            bill=self.bill,
            ad=self.ad,
            start_date=datetime.now()
        )

    def test_top_created(self):
        top = Top.objects.get(pk=1)
        self.assertEqual(top.ad.price, Decimal('99.99'))
        self.assertEqual(top.bill.invoice.user.username, 'Test')

    def test_top_create_not_unique(self):
        top = Top.objects.get(pk=1)
        with self.assertRaises(Exception):
            Top.objects.create(
                bill=self.bill,
                ad=self.ad,
                start_date=top.start_date
            )

    def test_top_finish_date_blank(self):
        top = Top.objects.get(pk=1)
        top.finish_date = ""
        with self.assertRaises(Exception):
            top.save()


class CurrencyTest(TestCase):
    def setUp(self):
        Currency.objects.create(code="RUR", rate=35.95)

    def test_currency_code(self):
        rur = Currency.objects.get(code="RUR")
        self.assertEqual(rur.code, "RUR")

    def test_currency_rate(self):
        rur = Currency.objects.get(code="RUR")
        self.assertEqual(rur.rate, Decimal('35.95'))

    def test_currency_unicode_call(self):
        rur = Currency.objects.get(code="RUR")
        self.assertEqual(rur.__unicode__(), "RUR")

    def test_currency_is_unique(self):
        with self.assertRaises(Exception):
            Currency.objects.create(code="RUR")

    def test_currency_is_not_null(self):
        with self.assertRaises(Exception):
            Currency.objects.create(code=None)

    def test_currency_is_not_empty(self):
        with self.assertRaises(Exception):
            Currency.objects.create(code="")

    def test_currency_too_long_code(self):
        with self.assertRaises(Exception):
            Currency.objects.create(code="TOO_LONG_CODE")

    def test_currency_rate_is_null(self):
        Currency.objects.create(code="KZT")
        kzt = Currency.objects.get(code="KZT")
        self.assertEqual(kzt.rate, None)

    def test_currency_rate_is_not_empty_string(self):
        with self.assertRaises(Exception):
            Currency.objects.create(code="KZT", rate="")


class UserContactTest(TestCase):
    test_user = None

    def setUp(self):
        self.test_user = User.objects.create(username='TestUser')
        UserContact.objects.create(phone_number='12345', user=self.test_user)

    def test_usercontact_created_defaults(self):
        uc = UserContact.objects.get(pk=1)
        self.assertEqual(uc.phone_number, '12345')
        self.assertEqual(uc.user.username, 'TestUser')

    def test_usercontact_create_null_phone_number(self):
        with self.assertRaises(Exception):
            UserContact.objects.create(phone_number=None, user=self.test_user)

    def test_usercontact_create_blank_phone_number(self):
        with self.assertRaises(Exception):
            UserContact.objects.create(phone_number="", user=self.test_user)

    def test_usercontact_create_same_phone_diff_user(self):
        uc = UserContact.objects.get(pk=1)
        user = User.objects.create(username='AnotherTestUser')
        self.assertTrue(
            UserContact.objects.create(
                phone_number=uc.phone_number,
                user=user
            )
        )

    def test_usercontact_create_same_phone_same_user(self):
        uc = UserContact.objects.get(pk=1)
        with self.assertRaises(Exception):
            UserContact.objects.create(
                phone_number=uc.phone_number,
                user=self.test_user
            )

    def test_usercontact_unicode_call(self):
        uc = UserContact.objects.get(pk=1)
        self.assertEqual(uc.__unicode__(), '12345 - TestUser')
