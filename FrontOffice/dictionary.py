# This Python file uses the following encoding: utf-8
AD_TYPE_VERBS = (
    ('SEL', 'Sell'),  # куплю
    ('BUY', 'Buy'),  # продам
    ('LTR', 'Long Term Rent'),  # сдам накоротко
    ('STR', 'Short Term Rent'),  # сдам надолго
    ('LTT', 'Long Term Take'),  # сниму надолго
    ('STT', 'Short Term Take'),  # сниму накоротко
    ('LTC', 'Long Term Change'),  # обменяюсь надолго
    ('STC', 'Short Term Change'),  # обменяюсь накоротко
)

AD_TYPE_NOUNS = (
    ('FLT', 'Flat'),  # квартира
    ('HOM', 'House'),  # дом
    ('GAR', 'Garage'),  # гараж
    ('DAC', 'Dacha'),  # дача
    ('PLT', 'Plot'),  # участок
    ('OFF', 'Office'),  # офис
    ('BLD', 'Building'),  # строение
    ('SHP', 'Shop'),  # магазин
    ('IBA', 'Industrial Base'),  # промбаза
    ('OTH', 'Other')  # другое
)

PROPERTIES_MAP = {
    # тип многоквартирного дома
    'type-of-apt-house': {
        'type': 'ENUM',
        'values': {
            '1': 'monolithic',  # монолитный
            '2': 'panel',  # панельный
            '3': 'brick',  # кирпичный
            '4': 'stalin-elite',  # сталинка элитная
            '5': 'stalin',  # сталинка
            '6': 'brick-monolithic',  # кирпично-монолитный
            '7': 'block',  # блочный
            '8': 'brezhnev',  # брежневка
            '9': 'khrusch',  # хрущовка
        },
        'ad_types': (
            ('SEL', 'FLT'),
            ('BUY', 'FLT'),
            ('LTR', 'FLT'),
            ('STR', 'FLT'),
            ('LTT', 'FLT'),
            ('STT', 'FLT'),
        ),
        'RUS': 'тип многоквартирного дома'
    },

    # тип жилого дома
    'type-of-living-house': {
        'type': 'ENUM',
        'values': {
            '1': 'panel',  # панельный
            '2': 'foam',  # пеноблочный
            '3': 'container',  # контейнерный
            '4': 'monolithic',  # монолит
            '5': 'cane',  # камыш
            '6': 'brick',  # кирпич
            '7': 'block',  # сруб
            '8': 'wood',  # деревянный
            '9': 'sandwich',  # сэндвич
            '10': 'cinder-block',  # шлакоблок
            '11': 'sleeper',  # шпальный
            '12': 'other'  # другое
        },
        'ad_types': (
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('LTR', 'HOM'),
            ('STR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTT', 'HOM'),
            ('STT', 'HOM'),
            ('STC', 'HOM'),
            ('LTC', 'DAC'),
        ),
        'RUS': 'тип жилого дома'
    },

    # состояние
    'condition': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),
            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),
            ('STR', 'FLT'),
            ('LTR', 'FLT'),
            ('STR', 'HOM'),
            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'OFF'),
            ('LTR', 'SHP'),
            ('LTR', 'BLD'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),
            ('LTT', 'FLT'),
            ('STT', 'FLT'),
            ('LTT', 'HOM'),
            ('STT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),
            ('STC', 'FLT'),
            ('LTC', 'FLT'),
            ('STC', 'HOM'),
            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),
        ),
        'RUS': 'состояние'
        #'values': range(1, 10)
    },

    # этаж
    'floor': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'OFF'),
            ('BUY', 'FLT'),
            ('LTR', 'FLT'),
            ('STR', 'FLT'),
            ('LTR', 'OFF'),
            ('LTT', 'FLT'),
            ('STT', 'FLT'),
            ('LTT', 'OFF'),
            ('LTC', 'FLT'),
            ('STC', 'FLT'),
            ('LTC', 'OFF'),
        ),
        'RUS': 'этаж'
    },

    # кол-во этажей
    'n-floor': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('BUY', 'FLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('SEL', 'HOM'),
            ('BUY', 'HOM'),
            ('STR', 'HOM'),
            ('LTR', 'HOM'),
            ('STT', 'HOM'),
            ('LTT', 'HOM'),
            ('STC', 'HOM'),
            ('LTC', 'HOM'),
        ),
        'RUS': 'кол-во этажей'
    },

    # кол-во комнат
    'n-rooms': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('STT', 'HOM'),
            ('LTT', 'HOM'),
            ('STT', 'FLT'),
            ('LTT', 'FLT'),
            ('LTC', 'FLT'),
            ('LTC', 'HOM'),
            ('STC', 'FLT'),
            ('STC', 'HOM'),
        ),
        'RUS': 'кол-во комнат'
    },

    # общая площадь
    'gross-area': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'FLT'),
        ),
        'RUS': 'общая площадь'
    },

    # полезная площадь
    'effective-area': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'FLT'),
        ),
        'RUS': 'полезная площадь'
    },

    # площадь кухни
    'kitchen-area': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'FLT'),
        ),
        'RUS': 'площадь кухни'
    },

    # площадь комм. недвижимости
    'commercial-estate-area': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),
            ('LTR', 'BLD'),
            ('LTR', 'SHP'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),
        ),
        'RUS': 'площадь комм. недвижимости'
    },

    # год постройки
    'year-of-bulding': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),
            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),
            ('STR', 'FLT'),
            ('LTR', 'FLT'),
            ('STR', 'HOM'),
            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'OFF'),
            ('LTR', 'SHP'),
            ('LTR', 'BLD'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),
            ('LTT', 'FLT'),
            ('STT', 'FLT'),
            ('LTT', 'HOM'),
            ('STT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),
            ('STC', 'FLT'),
            ('LTC', 'FLT'),
            ('STC', 'HOM'),
            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),
        ),
        'RUS': 'год постройки'
    },

    # наличие санузла
    'has-wc': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'FLT'),
        ),
        'RUS': 'наличие санузла'
    },

    # тип санузла
    'type-of-wc': {
        'type': 'ENUM',
        'values': {
            '1': 'separate',  # раздельный
            '2': 'united',  # совмещенный
        },
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'FLT'),
        ),
        'RUS': 'тип санузла'
    },

    # тип входной двери
    'type-of-entrance-door': {
        'type': 'ENUM',
        'values': {
            '1': 'wood',  # деревянная
            '2': 'metal',  # металлическая
            '3': 'armed'  # бронированная
        },
        'ad_types': (
            ('SEL', 'FLT'),
            ('BUY', 'FLT'),
            ('LTR', 'FLT'),
            ('STR', 'FLT'),
            ('LTT', 'FLT'),
            ('STT', 'FLT'),
            ('STC', 'FLT'),
            ('LTC', 'FLT'),

        ),
        'RUS': 'тип входной двери'
    },

    # высота потолков
    'height-of-ceiling': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('LTR', 'FLT'),
            ('LTR', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'FLT'),
            ('SEL', 'BLD'),
            ('BUY', 'BLD'),
            ('LTR', 'BLD'),
            ('LTC', 'BLD'),
            ('LTT', 'BLD'),
        ),
        'RUS': 'высота потолков'
    },

    # наличие лифта
    'has-elevator': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'FLT'),
            ('BUY', 'FLT'),
            ('LTR', 'FLT'),
            ('STR', 'FLT'),
            ('LTT', 'FLT'),
            ('STT', 'FLT'),
            ('STC', 'FLT'),
            ('LTC', 'FLT'),
            ('SEL', 'OFF'),
            ('BUY', 'OFF'),
            ('LTR', 'OFF'),
        ),
        'RUS': 'наличие лифта'
    },

    # наличие мебели
    'has-furniture': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'OFF'),
            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('LTR', 'OFF'),
            ('LTT', 'FLT'),
            ('STT', 'FLT'),
            ('LTT', 'HOM'),
            ('STT', 'HOM'),
            ('LTT', 'OFF'),
            ('STC', 'FLT'),
            ('STC', 'HOM'),
            ('LTC', 'OFF'),
        ),
        'RUS': 'наличие мебели'
    },

    # наличие телефона
    'has-telephone': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'PLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),
            ('LTR', 'SHP'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('STT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),

            ('STC', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'DAC'),
        ),
        'RUS': 'наличие телефона'
    },

    # тип телефонной линии
    'type-of-telephone-line': {
        'type': 'ENUM',
        'values': {
            '1': 'separate',  # отдельный
            '2': 'locker',  # блокиратор
        },
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'PLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),
            ('LTR', 'SHP'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('STT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),

            ('STC', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'DAC'),
        ),
        'RUS': 'тип телефонной линии'
    },

    # наличие интернета
    'has-internet': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'PLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),
            ('LTR', 'SHP'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('STT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),

            ('STC', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'DAC'),
        ),
        'RUS': 'наличие интернета'
    },

    # тип интернет-подключения
    'type-of-internet-line': {
        'type': 'ENUM',
        'values': {
            '1': 'adsl',  # ADSL
            '2': 'dial-up',  # телефон
            '3': 'optic',  # оптика
            '4': 'tv',  # тв-кабель
            '5': 'other',  # другое
        },
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'PLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),
            ('LTR', 'SHP'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('STT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),

            ('STC', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'DAC'),
        ),
        'RUS': 'тип интернет-подключения'
    },

    # наличие парковки
    'has-parking': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),

            ('BUY', 'FLT'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),

            ('LTR', 'FLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),

            ('STR', 'FLT'),

            ('LTT', 'FLT'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),

            ('STT', 'FLT'),

            ('LTC', 'FLT'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),

            ('STC', 'FLT'),
        ),
        'RUS': 'наличие парковки'
    },

    # кол-во парковочных мест
    'n-parking-places': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),

            ('BUY', 'FLT'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),

            ('LTR', 'FLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),

            ('STR', 'FLT'),

            ('LTT', 'FLT'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),

            ('STT', 'FLT'),

            ('LTC', 'FLT'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),

            ('STC', 'FLT'),
        ),
        'RUS': 'кол-во парковочных мест'
    },

    # наличие балкона
    'has-balcony': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'FLT'),
            ('BUY', 'FLT'),
            ('LTR', 'FLT'),
            ('STR', 'FLT'),
            ('LTT', 'FLT'),
            ('STT', 'FLT'),
            ('LTC', 'FLT'),
            ('STC', 'FLT'),
        ),
        'RUS': 'наличие балкона'
    },

    # тип балкона
    'type-of-balcony': {
        'type': 'ENUM',
        'values': {
            '1': 'loggia',
            '2': 'balcony'
        },
        'ad_types': (
            ('SEL', 'FLT'),
            ('BUY', 'FLT'),
            ('LTR', 'FLT'),
            ('STR', 'FLT'),
            ('LTT', 'FLT'),
            ('STT', 'FLT'),
            ('LTC', 'FLT'),
            ('STC', 'FLT'),
        ),
        'RUS': 'тип балкона'
    },

    # застеклен ли балкон
    'balcony-has-glass': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'FLT'),
            ('BUY', 'FLT'),
            ('LTR', 'FLT'),
            ('STR', 'FLT'),
            ('LTT', 'FLT'),
            ('STT', 'FLT'),
            ('LTC', 'FLT'),
            ('STC', 'FLT'),
        ),
        'RUS': 'застеклен ли балкон'
    },

    # тип пола
    'type-of-floor': {
        'type': 'ENUM',
        'values': {
            '1': 'lino',  # линолеум
            '2': 'parquet',  # паркет
            '3': 'laminate',  # ламинат
            '4': 'wood',  # дерево
            '5': 'tile',  # плитка
            '6': 'seramik',  # ковролан
            '7': 'cork',  # пробка
            '8': 'other'  # другое
        },
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),

            ('BUY', 'FLT'),
            ('BUY', 'HOM'),

            ('LTR', 'FLT'),
            ('LTR', 'HOM'),

            ('STR', 'FLT'),
            ('STR', 'HOM'),

            ('LTT', 'FLT'),
            ('LTT', 'HOM'),

            ('STT', 'FLT'),
            ('STT', 'HOM'),

            ('LTC', 'FLT'),
            ('LTC', 'HOM'),

            ('STC', 'FLT'),
            ('STC', 'HOM'),
        ),
        'RUS': 'тип пола'
    },

    # безопасность
    'security': {
        'type': 'ENUM',
        'values': {
            '1': 'littice',  # решетки
            '2': 'guard',  # охрана
            '3': 'combination-lock',  # кодовый замок
            '4': 'signaling',  # сигнализация
            '5': 'intercom',  # домофон
            '6': 'cctv',  # видеонаблюдение
            '7': 'concierge'  # консьерж
        },
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'PLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),
            ('LTR', 'SHP'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('STT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),

            ('STC', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'DAC'),
        ),
        'RUS': 'безопасность'
    },

    # материал покрытия крыши
    'roof-material': {
        'type': 'ENUM',
        'values': {
            '1': 'slate',  # шифер
            '2': 'bitoom-fiber-sheets',  # битумно-волокнистые листы
            '3': 'pvc',  # пвх
            '4': 'ruberoid',  # рубероид
            '5': 'metal',  # металлочерепица
            '6': 'corrugated-sheets',  # профильные листы
            '7': 'shingles',  # битумная черепица
            '8': 'tiles',  # керамическая черепица
            '9': 'cement-stand-tile',  # цементно-песчаная черепица
            '10': 'concrete-tile',  # бетонная черепица
            '11': 'polymer-tile',  # полимерпесчаная черепица
            '12': 'seam-roofing',  # фальцевая кровля
        },
        'ad_types': (
            ('SEL', 'HOM'),
            ('BUY', 'HOM'),
            ('LTR', 'HOM'),
            ('STR', 'HOM'),
            ('LTT', 'HOM'),
            ('STT', 'HOM'),
            ('LTC', 'HOM'),
            ('STC', 'HOM'),
        ),
        'RUS': 'материал покрытия крыши'
    },

    # сумма предоплаты
    'prepayment': {
        'type': 'NUM',
        'ad_types': (
            ('LTR', 'FLT'),
            ('LTR', 'HOM'),

            ('STR', 'FLT'),
            ('STR', 'HOM'),

            ('LTT', 'FLT'),
            ('LTT', 'HOM'),

            ('STT', 'FLT'),
            ('STT', 'HOM'),
        ),
        'RUS': 'сумма предоплаты'
    },

    # количество уровней
    'n-levels': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'HOM'),
            ('BUY', 'HOM'),
            ('LTR', 'HOM'),
            ('STR', 'HOM'),
            ('LTT', 'HOM'),
            ('STT', 'HOM'),
            ('LTC', 'HOM'),
            ('STC', 'HOM'),
        ),
        'RUS': 'количество уровней'
    },

    # наличие канализации
    'has-sewerage': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),
        ),
        'RUS': 'наличие канализации'
    },

    # наличие технической воды
    'has-technical-water': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'GAR'),
            ('SEL', 'DAC'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'GAR'),
            ('BUY', 'DAC'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'GAR'),
            ('LTR', 'DAC'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('LTT', 'GAR'),
            ('LTT', 'DAC'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('LTC', 'GAR'),
            ('LTC', 'DAC'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH')
        ),
        'RUS': 'наличие технической воды'
    },

    # наличие питьевой воды
    'has-drinking-water': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'OTH'),

            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'OTH'),

            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'OTH'),

            ('STR', 'HOM'),
            ('STR', 'DAC'),
            ('STR', 'OTH'),

            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'OTH'),

            ('STT', 'HOM'),
            ('STT', 'DAC'),
            ('STT', 'OTH'),

            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'OTH'),
        ),
        'RUS': 'наличие питьевой воды'
    },

    # наличие поливной воды
    'has-irrigation-water': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),

            ('BUY', 'DAC'),
            ('BUY', 'PLT'),

            ('LTR', 'DAC'),
            ('LTR', 'PLT'),

            ('LTT', 'DAC'),
            ('LTT', 'PLT'),

            ('STR', 'DAC'),
            ('STR', 'PLT'),

            ('STT', 'DAC'),
            ('STT', 'PLT'),

            ('LTC', 'DAC'),
            ('LTC', 'PLT'),

            ('STC', 'DAC'),
            ('STC', 'PLT'),
        ),
        'RUS': 'наличие поливной воды'
    },

    # наличие электричества
    'has-electricity': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'PLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),
            ('LTR', 'SHP'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('STT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),

            ('STC', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'DAC'),
        ),
        'RUS': 'наличие электричества'
    },

    # наличие газа
    'has-gas': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'PLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),
            ('LTR', 'SHP'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('STT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),

            ('STC', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'DAC'),
        ),
        'RUS': 'наличие газа'
    },

    # тип газовой проводки
    'type-of-gas-line': {
        'type': 'ENUM',
        'values': {
            '1': 'trunk',  # магистральный
            '2': 'autonomous'  # автономный
        },
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'PLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),
            ('LTR', 'SHP'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('STT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),

            ('STC', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'DAC'),
        ),
        'RUS': 'тип газовой проводки'
    },

    # наличие гаража
    'has-garage': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'HOM'),
            ('BUY', 'HOM'),
            ('LTR', 'HOM'),
            ('STR', 'HOM'),
            ('LTT', 'HOM'),
            ('STT', 'HOM'),
            ('LTC', 'HOM'),
            ('STC', 'HOM'),
        ),
        'RUS': 'наличие гаража'
    },

    # площадь земельного участка
    'land-area': {
        'type': 'NUM',
        'ad_types': None,
        'RUS': 'площадь земельного участка'
    },

    # тип гаража
    'type-of-garage': {
        'type': 'ENUM',
        'values': {
            '1': 'wood',  # деревянный
            '2': 'monolithic',  # монолит
            '3': 'brick',  # кирпичный
            '4': 'metal',  # металлический
            '5': 'block',  # блочный
            '6': 'other',  # другое
        },
        'ad_types': (
            ('SEL', 'GAR'),
            ('BUY', 'GAR'),
            ('LTR', 'GAR'),
            ('LTT', 'GAR'),
            ('LTC', 'GAR'),
        ),
        'RUS': 'тип гаража'
    },

    # наличие охраны
    'has-guard': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'FLT'),
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),
            ('SEL', 'OFF'),
            ('SEL', 'BLD'),
            ('SEL', 'SHP'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'FLT'),
            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),
            ('BUY', 'OFF'),
            ('BUY', 'BLD'),
            ('BUY', 'SHP'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'FLT'),
            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'PLT'),
            ('LTR', 'OFF'),
            ('LTR', 'BLD'),
            ('LTR', 'SHP'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('STR', 'FLT'),
            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'FLT'),
            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),
            ('LTT', 'OFF'),
            ('LTT', 'BLD'),
            ('LTT', 'SHP'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('STT', 'FLT'),
            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'FLT'),
            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),
            ('LTC', 'OFF'),
            ('LTC', 'BLD'),
            ('LTC', 'SHP'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),

            ('STC', 'FLT'),
            ('STC', 'HOM'),
            ('STC', 'DAC'),
        ),
        'RUS': 'наличие охраны'
    },

    # наличие погреба
    'has-cellar': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'GAR'),
            ('BUY', 'GAR'),
            ('LTR', 'GAR'),
            ('LTT', 'GAR'),
            ('LTC', 'GAR'),
        ),
        'RUS': 'наличие погреба'
    },

    # наличие ямы
    'has-pit': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'GAR'),
            ('BUY', 'GAR'),
            ('LTR', 'GAR'),
            ('LTT', 'GAR'),
            ('LTC', 'GAR'),
        ),
        'RUS': 'наличие ямы'
    },

    # кол-во въездов
    'n-entries': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'GAR'),
            ('BUY', 'GAR'),
            ('LTR', 'GAR'),
            ('LTT', 'GAR'),
            ('LTC', 'GAR'),
        ),
        'RUS': 'кол-во въездов'
    },

    # делимый, да-нет (земельный участок)
    'is-divided': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'HOM'),
            ('SEL', 'PLT'),

            ('BUY', 'HOM'),
            ('BUY', 'PLT'),

            ('LTR', 'HOM'),
            ('LTR', 'PLT'),

            ('LTT', 'HOM'),
            ('LTT', 'PLT'),

            ('STR', 'HOM'),
            ('STR', 'PLT'),

            ('STT', 'HOM'),
            ('STT', 'PLT'),

            ('LTC', 'HOM'),
            ('LTC', 'PLT'),

            ('STC', 'HOM'),
            ('STC', 'PLT'),
        ),
        'RUS': 'делимый, да-нет (земельный участок)'
    },

    # тип собственности на землю
    'type-of-ownership-of-land': {
        'type': 'ENUM',
        'values': {
            '1': 'private',  # частная
            '2': 'rented',  # аренда
            '3': 'sub-rented',  # субаренда
        },
        'ad_types': (
            ('SEL', 'HOM'),
            ('SEL', 'GAR'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'HOM'),
            ('BUY', 'GAR'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'HOM'),
            ('LTR', 'GAR'),
            ('LTR', 'DAC'),
            ('LTR', 'PLT'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('LTT', 'HOM'),
            ('LTT', 'GAR'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('LTC', 'HOM'),
            ('LTC', 'GAR'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),
        ),
        'RUS': 'тип собственности на землю'
    },

    # наличие дачного дома
    'has-dacha-house': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'DAC'),
            ('BUY', 'DAC'),
            ('LTR', 'DAC'),
            ('STR', 'DAC'),
            ('LTT', 'DAC'),
            ('STT', 'DAC'),
            ('LTC', 'DAC'),
        ),
        'RUS': 'наличие дачного дома'
    },

    # наличие надворных построек
    'has-outbuildings': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),

            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),

            ('LTR', 'HOM'),
            ('LTR', 'DAC'),
            ('LTR', 'PLT'),

            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),

            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),
        ),
        'RUS': 'наличие надворных построек'
    },

    # тип земельного участка
    'type-of-land': {
        'type': 'ENUM',
        'values': {
            '1': 'in-city',  # в городе
            '2': 'in-suburb',  # в пригороде
            '3': 'aling-of-track',  # вдоль трассы
            '4': 'near-pond',  # возле водоема
            '5': 'near-river',  # возле реки
            '6': 'in-foothills',  # в предгорьях
            '7': 'in-dacha-array',  # в дачном массиве
            '8': 'other',  # другое
        },
        'ad_types': (
            ('SEL', 'HOM'),
            ('SEL', 'PLT'),
            ('SEL', 'OTH'),

            ('BUY', 'HOM'),
            ('BUY', 'PLT'),
            ('BUY', 'OTH'),

            ('LTR', 'HOM'),
            ('LTR', 'PLT'),
            ('LTR', 'OTH'),

            ('LTT', 'HOM'),
            ('LTT', 'PLT'),
            ('LTT', 'OTH'),

            ('STR', 'HOM'),
            ('STR', 'PLT'),
            ('STR', 'OTH'),

            ('STT', 'HOM'),
            ('STT', 'PLT'),
            ('STT', 'OTH'),

            ('LTC', 'HOM'),
            ('LTC', 'PLT'),
            ('LTC', 'OTH'),

            ('STC', 'HOM'),
            ('STC', 'PLT'),
            ('STC', 'OTH'),
        ),
        'RUS': 'тип земельного участка'
    },

    # класс офисного здания
    'class-of-office-bulding': {
        'type': 'ENUM',
        'values': {
            '1': 'A1',  # А1
            '2': 'A2',  # А2
            '3': 'A3',  # А3
            '4': 'B',  # В
            '5': 'C1',  # С1
            '6': 'C2',  # С2
            '7': 'D',  # D
            '8': 'other',  # другое
        },
        'ad_types': (
            ('SEL', 'OFF'),
            ('BUY', 'OFF'),
            ('LTR', 'OFF'),
            ('LTT', 'OFF'),
            ('LTC', 'OFF'),
        ),
        'RUS': 'класс офисного здания'
    },

    # тип строения
    'type-of-building': {
        'type': 'ENUM',
        'values': {
            '1': 'brick',  # кирпич
            '2': 'monolithic',  # монолит
            '3': 'wood',  # дерево
            '4': 'panel',  # панель
            '5': 'other',  # другое
        },
        'ad_types': (
            ('SEL', 'GAR'),
            ('SEL', 'BLD'),
            ('SEL', 'IBA'),
            ('SEL', 'OTH'),

            ('BUY', 'GAR'),
            ('BUY', 'BLD'),
            ('BUY', 'IBA'),
            ('BUY', 'OTH'),

            ('LTR', 'GAR'),
            ('LTR', 'BLD'),
            ('LTR', 'IBA'),
            ('LTR', 'OTH'),

            ('LTT', 'GAR'),
            ('LTT', 'BLD'),
            ('LTT', 'IBA'),
            ('LTT', 'OTH'),

            ('LTC', 'GAR'),
            ('LTC', 'BLD'),
            ('LTC', 'IBA'),
            ('LTC', 'OTH'),
        ),
        'RUS': 'тип строения'
    },

    # количество телефонных линий
    'n-telephone-lines': {
        'type': 'NUM',
        'ad_types': None,
        'RUS': 'количество телефонных линий'
    },

    # местоположение магазина
    'location-of-shop': {
        'type': 'ENUM',
        'values': {
            '1': 'mall',  # ТЦ
            '2': 'municipal-building',  # адм. здание
            '3': 'market',  # рынок
            '4': 'living-house',  # жилое здание
            '5': 'separate-building',  # отдельное здание
            '6': 'bus-stop',  # остановка
            '7': 'other',  # другое
        },
        'ad_types': (
            ('SEL', 'SHP'),
            ('BUY', 'SHP'),
            ('LTR', 'SHP'),
            ('LTT', 'SHP'),
            ('LTC', 'SHP'),
        ),
        'RUS': 'местоположение магазина'
    },

    # тип промбазы
    'type-of-industrial-base': {
        'type': 'ENUM',
        'values': {
            '1': 'industrial-base',  # промбаза
            '2': 'appliance-warehouse',  # склад бытовой
            '3': 'food-warehouse',  # склад продов.
            '4': 'chemical-warehouse',  # хим. склад
            '5': 'plan',  # завод
            '6': 'other',  # другое
        },
        'ad_types': (
            ('SEL', 'IBA'),
            ('BUY', 'IBA'),
            ('LTR', 'IBA'),
            ('LTT', 'IBA'),
            ('LTC', 'IBA'),
        ),
        'RUS': 'тип промбазы'
    },

    #  высота потолков произв. помещений
    'height-of-ceilings-of-production-rooms': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'IBA'),
            ('BUY', 'IBA'),
            ('LTR', 'IBA'),
            ('LTT', 'IBA'),
            ('LTC', 'IBA'),
        ),
        'RUS': 'высота потолков произв. помещений'
    },

    # высота потолков складских помещений
    'heigth-of-seilings-of-warehouse-rooms': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'IBA'),
            ('BUY', 'IBA'),
            ('LTR', 'IBA'),
            ('LTT', 'IBA'),
            ('LTC', 'IBA'),
        ),
        'RUS': 'высота потолков складских помещений'
    },

    # наличие жд тупика
    'has-railway-deadlock': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'IBA'),
            ('BUY', 'IBA'),
            ('LTR', 'IBA'),
            ('LTT', 'IBA'),
            ('LTC', 'IBA'),
        ),
        'RUS': 'наличие жд тупика'
    },

    # макс потребление энергии
    'max-of-power-consumption': {
        'type': 'NUM',
        'ad_types': (
            ('SEL', 'IBA'),
            ('BUY', 'IBA'),
            ('LTR', 'IBA'),
            ('LTT', 'IBA'),
            ('LTC', 'IBA'),
        ),
        'RUS': 'макс потребление энергии'
    },

    # наличие своей подстанции
    'has-own-substantion': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'IBA'),
            ('BUY', 'IBA'),
            ('LTR', 'IBA'),
            ('LTT', 'IBA'),
            ('LTC', 'IBA'),
        ),
        'RUS': 'наличие своей подстанции'
    },

    # сфера деятельности предприятия
    'sphere-of-activity-of-enterprise': {
        'type': 'ENUM',
        'values': {
            '1': 'production',  # производство
            '2': 'trade',  # торговля
            '3': 'public-catering',  # общепит
            '4': 'services',  # услуги
            '5': 'intertaiment',  # развлечения
            '6': 'agriculture',  # с/х
            '7': 'other',  # другое
        },
        'ad_types': (
            ('SEL', 'OTH'),
            ('BUY', 'OTH'),
            ('LTR', 'OTH'),
            ('LTT', 'OTH'),
            ('LTC', 'OTH'),
        ),
        'RUS': 'сфера деятельности предприятия'
    },

    # наличие действ. бизнеса
    'has-functional-business': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'OTH'),
            ('SEL', 'SHP'),

            ('BUY', 'OTH'),
            ('BUY', 'SHP'),

            ('LTR', 'OTH'),
            ('LTR', 'SHP'),

            ('LTT', 'OTH'),
            ('LTT', 'SHP'),

            ('LTC', 'OTH'),
            ('LTC', 'SHP'),
        ),
        'RUS': 'наличие действ. бизнеса'
    },

    # наличие отдельного входа
    'has-own-entrance': {
        'type': 'BOOL',
        'ad_types': None,
        'RUS': 'наличие отдельного входа'
    },

    # местоположение промбазы, склада, завода
    'location-of-industrial-base': {
        'type': 'ENUM',
        'values': {
            '1': 'in-city',  # в городе
            '2': 'in-suburb',  # в пригороде
            '3': 'along-track',  # вдоль трассы
        },
        'ad_types': None,
        'RUS': 'местоположение промбазы, склада, завода'
    },

    # местоположение - прочая недвижимость
    'location-of-other-estate': {
        'type': 'ENUM',
        'values': {
            '1': 'in-city',  # в городе
            '2': 'in-suburb',  # в пригороде
            '3': 'aling-of-track',  # вдоль трассы
            '4': 'near-pond',  # возле водоема
            '5': 'near-river',  # возле реки
            '6': 'in-foothills',  # в предгорьях
            '7': 'in-dacha-array',  # в дачном массиве
            '8': 'other',  # другое
        },
        'ad_types': None,
        'RUS': 'местоположение - прочая недвижимость'
    },

    # кол-во спальных мест
    'n-sleep-places': {
        'type': 'NUM',
        'ad_types': None,
        'RUS': 'кол-во спальных мест'
    },

    # период обмена
    'period-of-exchange': {
        'type': 'NUM',
        'ad_types': (
            ('STC', 'FLT'),
            ('STC', 'HOM'),
        ),
        'RUS': 'период обмена'
    },

    # доплата при обмене
    'supplement-of-exchange': {
        'type': 'NUM',
        'ad_types': (
            ('LTC', 'FLT'),
            ('LTC', 'DAC'),
            ('LTC', 'GAR'),
            ('LTC', 'HOM'),
            ('LTC', 'OFF'),
            ('LTC', 'IBA'),
            ('LTC', 'BLD'),
            ('LTC', 'OTH'),
            ('LTC', 'PLT')
        ),
        'RUS': 'доплата при обмене'
    },

    # хозпостройки
    'has-houshold-constructions': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),
            ('SEL', 'PLT'),

            ('BUY', 'HOM'),
            ('BUY', 'DAC'),
            ('BUY', 'PLT'),

            ('LTR', 'HOM'),
            ('LTR', 'DAC'),

            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'HOM'),
            ('LTT', 'DAC'),
            ('LTT', 'PLT'),

            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'HOM'),
            ('LTC', 'DAC'),
            ('LTC', 'PLT'),

            ('STT', 'HOM'),
            ('STT', 'DAC'),
            ('STT', 'PLT'),
        ),
        'RUS': 'хозпостройки'
    },

    # баня
    'has-bath': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),

            ('BUY', 'HOM'),
            ('BUY', 'DAC'),

            ('LTR', 'HOM'),
            ('LTR', 'DAC'),

            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'HOM'),
            ('LTT', 'DAC'),

            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'HOM'),
            ('LTC', 'DAC'),

            ('STT', 'HOM'),
            ('STT', 'DAC'),
        ),
        'RUS': 'баня'
    },

    # бассейн
    'has-pool': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),

            ('BUY', 'HOM'),
            ('BUY', 'DAC'),

            ('LTR', 'HOM'),
            ('LTR', 'DAC'),

            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'HOM'),
            ('LTT', 'DAC'),

            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'HOM'),
            ('LTC', 'DAC'),

            ('STT', 'HOM'),
            ('STT', 'DAC'),
        ),
        'RUS': 'бассейн'
    },

    # навес
    'has-canopy': {
        'type': 'BOOL',
        'ad_types': (
            ('SEL', 'HOM'),
            ('SEL', 'DAC'),

            ('BUY', 'HOM'),
            ('BUY', 'DAC'),

            ('LTR', 'HOM'),
            ('LTR', 'DAC'),

            ('STR', 'HOM'),
            ('STR', 'DAC'),

            ('LTT', 'HOM'),
            ('LTT', 'DAC'),

            ('STT', 'HOM'),
            ('STT', 'DAC'),

            ('LTC', 'HOM'),
            ('LTC', 'DAC'),

            ('STT', 'HOM'),
            ('STT', 'DAC'),
        ),
        'RUS': 'навес'
    },

    # расстояние до ближайшего города
    'distance-to-nearest-town': {
        'type': 'NUM',
        'ad_types': None,
        'RUS': 'расстояние до ближайшего города'
    }
}
