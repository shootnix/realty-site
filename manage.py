#!/usr/bin/env python
import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "RealtySite.settings")

from FrontOffice.models import Property, AdType
from FrontOffice.dictionary import PROPERTIES_MAP, AD_TYPE_NOUNS, AD_TYPE_VERBS


def popdb():
    print("Pop AdTypes")
    for noun in (k for k, v in AD_TYPE_NOUNS):
        for verb in (k for k, v in AD_TYPE_VERBS):
            try:
                AdType.objects.create(
                    verb=verb,
                    noun=noun
                )
            except:
                print " ERROR FOR %s - %s" % (verb, noun)

    for property_name in PROPERTIES_MAP.keys():
        print("Creating property '%s'" % property_name)
        if PROPERTIES_MAP[property_name]['ad_types'] is not None:
            ad_types_list = PROPERTIES_MAP[property_name]['ad_types']
            ad_types = list()
            try:
                prop = Property.objects.create(
                    name=property_name,
                    cont_type=PROPERTIES_MAP[property_name]['type'],
                    RUS=PROPERTIES_MAP[property_name]['RUS']
                )
                for ad_type_pairs in ad_types_list:
                    ad_type = AdType.objects.get(
                        verb=ad_type_pairs[0],
                        noun=ad_type_pairs[1]
                    )
                    prop.ad_types.add(ad_type)
            except:
                print " ERROR"

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "RealtySite.settings")

    from django.core.management import execute_from_command_line

    CUSTOM_COMMANDS = ('popdb')
    if sys.argv[1] in CUSTOM_COMMANDS:
        if sys.argv[1] == 'popdb':
            popdb()
    else:
        execute_from_command_line(sys.argv)

    if sys.argv[1] == 'syncdb':
        popdb()
