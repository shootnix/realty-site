# This Python file uses the following encoding: utf-8

#!/usr/bin/env python
import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "RealtySite.settings")

from FrontOffice.models import Property, AdType
from FrontOffice.dictionary import PROPERTIES_MAP, AD_TYPE_NOUNS, AD_TYPE_VERBS

all_props = Property.objects.all()
print("Все доступные свойства:")
for prop in all_props:
    print("* %s" % (prop.RUS.encode('utf-8')))

print('------------')

ad_types = AdType.objects.all()
for ad_type in ad_types:
    print("%s:" % (ad_type))
    props = Property.objects.filter(ad_types=ad_type)
    for prop in props:
        print(" - %s" % (prop.RUS.encode('utf-8')))
