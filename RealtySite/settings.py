"""
Django settings for RealtySite project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 's68hiu*88wu8$md(p%c&1uy=^=)$h_82h_bnw63f8h71e%hm&w'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'suit',
    #'admin_tools',
    #'admin_tools.theming',
    #'admin_tools.menu',
    #'admin_tools.dashboard',
    'django.contrib.auth',
    'django.contrib.sites',
    #'django.contrib.admin'
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'FrontOffice'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'RealtySite.urls'

WSGI_APPLICATION = 'RealtySite.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

USE_I18N = True

USE_L10N = True

USE_TZ = False

SITE_ID = 1


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'


from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
)

SUIT_CONFIG = {

    'MENU': (
        'site',
        {
            'app': 'auth',
            'label': 'Authorization',
            'icon':'icon-lock'
        },
        {
            'label': 'Settings',
            'icon': 'icon-cog',
            'models': (
                'FrontOffice.language',
                'FrontOffice.currency'
            )
        },
        {
            'label': 'Finances',
            'icon': 'icon-thumbs-up',
            'models': (
                'FrontOffice.service',
                'FrontOffice.invoice',
                'FrontOffice.bill',
            )
        },
        {
            'label': 'Advertising',
            'icon': 'icon-th',
            'models': (
                'FrontOffice.ad',
                'FrontOffice.adproperty',
                'FrontOffice.top',
                'FrontOffice.photo',
                'FrontOffice.youtubevideo'
            )
        },
        {
            'label': 'Advertising - Settings',
            'icon': 'icon-wrench',
            'models': (
                'FrontOffice.adtype',
                'FrontOffice.property',
                'FrontOffice.tag'
            )
        },
        {
            'label': 'Geo',
            'icon': 'icon-map-marker',
            'models': (
                'FrontOffice.country',
                'FrontOffice.city',
                'FrontOffice.district'
            )
        }
    )
}