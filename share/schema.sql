-- Money money money

create table invoice (
    id serial not null primary key,

    create_date  timestamp not null default now(),
    mod_date     timestamp null default null,

    discount  integer not null default 0,
    istatus   integer not null default 0, --- 0 - CREATED, 1 - PENDING, 2 - PAYED, 3 - CANCELLED, 4 - REJECTEDs

    user_id   integer not null references user(id)
);

create table service (
    id     serial not null primary key,
    name   varchar(50),
    price  real not null default 0
);

create table bill (
    id serial not null primary key,

    create_date  timestamp not null default now(),
    mod_date     timestamp null default null,
    
    discount    integer not null default 0

    service_id  integer not null references service(id),
    invoice_id  integer not null references invoice(id)
);

--- Address
create table country (
    id    serial not null primary key,
    code  char(2) not null unique
);

create table city (
    id          serial not null primary key,
    name        varchar(100),
    country_id  integer not null references country(id)
);

create table district (
    id       serial not null primary key,
    name     varchar(100),
    city_id  integer not null references city(id)
);

--- lang
create table lang (
    id    serial not null primary key,
    code  char(3)
);

--- Ad tables
create table ad_type (
    id    serial not null primary key,
    verb  varchar(15) not null,
    noun  varchar(25) not null
);

create table ad (
    -- meta info
    id serial  not null primary key,
    summary    varchar(100) not null,
    pub_date   timestamp not null default now(),
    mod_date   timestamp null default null,
    moderated  boolean not null default false,

    -- common fields
    price            float null default null,
    misc             varchar(100),
    has_arrest       integer null default null,
    has_pledge       integer null default null,
    has_encumbrance  integer null default null,

    -- foreign keys
    district_id  integer not null references district(id),
    ad_type_id   integer not null references ad_type(id),
    lang_id      integer not null references lang(id),
    user_id      integer not null references user(id)
);

create table prop (
    id        serial not null primary key,
    name      varchar(20),
    conttype  enum('number', 'bool', 'char')
);
create table ad_prop (
    ad_id     integer not null references ad(id),
    prop_id   integer not null references prop(id),
    prop_val  varchar(20)
);

-- Pure Many-to-Many
create table ad_type_prop (
    prop_id     integer not null references prop(id),
    ad_type_id  integer not null references ad_type(id)
);

create table photo_ad (
    id        serial not null primary key,
    filename  char(32),
    ad_id     integer not null references ad(id)
);

create table youtube_video_ad (
    id      serial not null primary key,
    widget  text,
    ad_id   integer not null references ad(id)
);

create table tag (
    id     serial not null primary key,
    name   varchar(20),
    price  decimal(10,2)
);

-- Pure Many-to-Many
create table ad_tag (
    ad_id   integer not null primary key,
    tag_id  integer not null primary key
);

create table top_ad (
    id integer  not null primary key,
    pub_date    timestamp not null default now(),
    pub_close   timestamp null default null,

    ad_id       integer not null references ad(id),
    bill_id     integer not null references bill(id)
);

-- Other

create table currency (
    id    serial not null primary key,
    code  char(3) not null unique,
    rate  float not null default 0
);

create table user_info (
    id           serial not null primary key,
    description  text null default null,
    -- contact's data
    tel         varchar(25) null default null,
    mob         varchar(25) null default null,
    skype       varchar(25) null default null
    user_id     integer not null references user(id)
);

create table user_contacts (
    id  serial not null primary key,
    phone_number varchar(50),

    user_id  integer not null references user(id)
)

create table user_foreign_accounts (
    id             serial not null primary key,
    vk             varchar(20),
    fb             varchar(60),
    twitter        varchar(50),
    instagramm     varchar(50),
    odnoklassniki  varchar(50)
);

